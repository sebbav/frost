/*
Arduino Library to use a Mux/demux 4051
http://www.datasheetcatalog.com/datasheets_pdf/C/D/4/0/CD4051.shtml

-enablePin(uint8_t pin): receive the number of the pin you want to select
	from 0 to 7, tatal 8 pin; for pin numbers read the datasheet.
-readAnalog(uint8_t pin): return the analogRead of the pin given.
-writePWM(uint8_t pin, uint8_t val): analogWrite on the given pin
	of the value 'val' from 0 to 255.
-writeDigital(uint8_t pin, uint8_t val): write HIGH or LOW to the given pin.
-readDigital(uint8_t pin): return the digital value (HIGH or LOW) of the given pin.
-turnPinOn(uint8_t pin): send HIGH on the given pin.
-turnPinOff(uint8_t pin): send LOW on the given pin.
-sendHigh(): send HIGH on the active pin.
-sendLow(): send LOW on the active pin.

Author: Stefano Moioli
e-mail: smoioli@gmail.com
*/

#include "Arduino.h"
#include "Mux4051.h"

Mux4051::Mux4051(uint8_t s0, uint8_t s1, uint8_t s2, uint8_t z){
	_s0 = s0;
	_s1 = s1;
	_s2 = s2;
	_z = z;
	
	pinMode(_s0, OUTPUT); //set the address line as OUTPUT
	pinMode(_s1, OUTPUT);
	pinMode(_s2, OUTPUT);
}

void Mux4051::enablePin(uint8_t pin){
	if(pin<0)	pin = 0;
	if(pin>7)	pin = 7;
	
	digitalWrite(_s0, bitRead(pin, 0));
	digitalWrite(_s1, bitRead(pin, 1));
	digitalWrite(_s2, bitRead(pin, 2));
	
	delay(1); //need a little time to select pin
}

uint8_t Mux4051::readDigital(uint8_t pin){
	sendLow();
	pinMode(_z, INPUT);
	enablePin(pin);
	return digitalRead(_z);
}

void Mux4051::writeDigital(uint8_t pin, uint8_t val){
	enablePin(pin);
	pinMode(_z, OUTPUT);
	digitalWrite(_z, 1);
}

int Mux4051::readAnalog(uint8_t pin){
	sendLow();
	pinMode(_z, INPUT);
	enablePin(pin);
	return analogRead(_z);
}

void Mux4051::writePWM(uint8_t pin, uint8_t val){
	enablePin(pin);
	pinMode(_z, OUTPUT);
	analogWrite(_z, val);
}

void Mux4051::turnPinOn(uint8_t pin){
	sendHigh();
	enablePin(pin);
}

void Mux4051::turnPinOff(uint8_t pin){
	sendLow();
	enablePin(pin);
}

void Mux4051::sendHigh(){
	pinMode(_z, OUTPUT);
	digitalWrite(_z, HIGH);
}

void Mux4051::sendLow(){
	pinMode(_z, OUTPUT);
	digitalWrite(_z, LOW);
}