/*
Arduino Library to use a Mux/demux 4051
http://www.datasheetcatalog.com/datasheets_pdf/C/D/4/0/CD4051.shtml

-enablePin(uint8_t pin): receive the number of the pin you want to select
	from 0 to 7, tatal 8 pin; for pin numbers read the datasheet.
-readAnalog(uint8_t pin): return the analogRead of the pin given.
-writePWM(uint8_t pin, uint8_t val): analogWrite on the given pin
	of the value 'val' from 0 to 255.
-writeDigital(uint8_t pin, uint8_t val): write HIGH or LOW to the given pin.
-readDigital(uint8_t pin): return the digital value (HIGH or LOW) of the given pin.
-turnPinOn(uint8_t pin): send HIGH on the given pin.
-turnPinOff(uint8_t pin): send LOW on the given pin.
-sendHigh(): send HIGH on the active pin.
-sendLow(): send LOW on the active pin.

Author: Stefano Moioli
e-mail: smoioli@gmail.com
*/
#ifndef Mux4051_h
#define Mux4051_h

#include "Arduino.h"

	class Mux4051
	{
		public:
			Mux4051(uint8_t s0, uint8_t s1, uint8_t s2, uint8_t z);
			void enablePin(uint8_t pin); //A
			int readAnalog(uint8_t pin); //B
			uint8_t readDigital(uint8_t pin); //C
			void writePWM(uint8_t pin, uint8_t val); //D
			void writeDigital(uint8_t pin, uint8_t val); //E
			void turnPinOn(uint8_t pin); //F
			void turnPinOff(uint8_t pin); //G
			void sendHigh(); //H
			void sendLow(); //I
		private:
			uint8_t _s0;
			uint8_t _s1;
			uint8_t _s2;
			uint8_t _z;
	};
#endif