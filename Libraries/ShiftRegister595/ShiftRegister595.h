#ifndef ShiftRegister595_h
#define ShiftRegister595_h

#include "Arduino.h"

class ShiftRegister595 {
public:
	ShiftRegister595(uint8_t latchPin, uint8_t dataPin, uint8_t clockPin, uint8_t numberChips, uint8_t direction = MSBFIRST, bool off = true);
	void init();
	void writePin(uint16_t Pin, uint8_t whichState);
	void setPin(uint16_t Pin, uint8_t whichState);
	void start();
	void end();
	uint16_t getMaxOut();

private:
	uint8_t _latchPin;
	uint8_t _dataPin;
	uint8_t _clockPin;
	uint8_t _numberChips;
	uint8_t _direction;
	uint16_t _maxOut;
	uint8_t _arByte[8];
	bool _off;
};
#endif
