/*
 */

#include "Arduino.h"
#include "ShiftRegister595.h"

ShiftRegister595::ShiftRegister595(uint8_t latchPin, uint8_t dataPin, uint8_t clockPin, uint8_t numberChips, uint8_t direction, bool off) {
	_latchPin = latchPin;
	_dataPin = dataPin;
	_clockPin = clockPin;
	_numberChips = numberChips;
	_direction = direction;
	_maxOut = _numberChips * 8;
	_off = off;

	pinMode(_latchPin, OUTPUT);
	pinMode(_dataPin, OUTPUT);
	pinMode(_clockPin, OUTPUT);
	ShiftRegister595::init();

}
void ShiftRegister595::init() {
	byte state = 0x0;
	digitalWrite(_latchPin, HIGH);
	if (_off)
		state = 0xFF;
	digitalWrite(_latchPin, LOW);

	for (byte i = 0; i <= _numberChips; i++) {
		_arByte[i] = state;
		shiftOut(_dataPin, _clockPin, _direction, _arByte[i]);
	}

	digitalWrite(_latchPin, HIGH);
}

void ShiftRegister595::writePin(uint16_t Pin, uint8_t whichState) {
	uint8_t i = (Pin - 1) / 8;
	if (_off)
		whichState = !whichState;

	digitalWrite(_latchPin, LOW);
	bitWrite(_arByte[i], ((Pin - 1) % 8), whichState);

	for (int j = _numberChips - 1; j >= 0; j--) {
		shiftOut(_dataPin, _clockPin, _direction, _arByte[j]);
	}

	digitalWrite(_latchPin, HIGH);
}

uint16_t ShiftRegister595::getMaxOut() {
	return _maxOut;
}

void ShiftRegister595::setPin(uint16_t Pin, uint8_t whichState) {
	uint16_t i = (Pin - 1) / 8;
	if (_off)
		whichState = !whichState;
	bitWrite(_arByte[i], ((Pin - 1) % 8), whichState);
}

void ShiftRegister595::start() {
	digitalWrite(_latchPin, LOW);
}

void ShiftRegister595::end() {
	for (int j = _numberChips - 1; j >= 0; j--)
		shiftOut(_dataPin, _clockPin, _direction, _arByte[j]);
	digitalWrite(_latchPin, HIGH);
}
