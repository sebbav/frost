// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _frost_H_
#define _frost_H_
#include "Arduino.h"
//add your includes for the project frost here


//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project frost here
void readDataFromEEPROM();
void clearRAM();
void startFan();
void startSol();

bool drawNumPad();
void drawFan(int x, int y, int active = 0);
void drawSolenoid(int x, int y, bool active = false);
void drawSnowFlake(int x, int y, byte active = 0);
void drawSetup();
void drawButtonOnOff();
void draw_interface();
void drawAlarm(byte zone, byte level, boolean alarm = false);
void drawTemp(byte zone, float temp, byte level = 1);
void drawAverageTemp(byte zone);

int checkSolenoid(byte i);
void tuning(byte zone);
int getZone(int x);
byte getLevel(int y);

float calcTemp(uint8_t* data, byte resolution = 9);

void switchSolenoid(byte zone, byte status, bool draw=true);
void switchFan(byte zone, byte status, bool draw=true);
//Do not add code below this line
#endif /* _frost_H_ */
