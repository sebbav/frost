#include "frost.h"
#include "UTFT.h"
#include "UTFT_Geometry.h"
#include "UTouch.h"
#include "UTFT_Buttons.h"
#include "Timer.h"
#include "OneWire.h"
#include "Mux4051.h"
#include "ShiftRegister595.h"
#include "SPIflash.h"

#define TOUCH_ORIENTATION  LANDSCAPE
//#define SD_PIN 53 //JP8 onboard SD card
extern uint8_t SmallFont[];
//extern uint8_t BigFont[];
extern uint8_t Ubuntu[];
extern uint8_t DotMatrix_M[];
extern uint8_t franklingothic_normal[];

#define C_ACTIVE 0x86F0
#define C_INACTIVE 0x2104
#define C_BG VGA_BLACK
#define C_BLUE 0x141F
#define C_CYAN 0xCFFF
#define C_RED 0x9000
#define C_ORANGE 0xFC22
#define C_YELLOW 0xFFE0

#define L_C_1 100
#define L_C_2 200
#define L_C_3 270
ShiftRegister595 sr595(8, 9, 10, 4, MSBFIRST, false);
UTFT oTFT(CTE70, 25, 26, 27, 28);
UTFT_Geometry oGeo(&oTFT);
UTouch oTouch(6, 5, 4, 3, 2);
UTFT_Buttons oBut(&oTFT, &oTouch);
Timer t;
SPIflash oFlash(52);
bool _startFan = false, _startSolenoid = false, _working = false, _start_ = true, _isProcessing = false, _isOverHeatingTime = false;
float _hysteresis = 5;
byte _maxHeatTime = 3, _maxOverHeatingTime = 15, _startOverHeatingTimePause = 20; //в минутах
uint32_t _startOverHeatingTime;
/******************  ******* ******************************/
const char _PASS[6] = { '7', '3', '6', '5', '6', '2' };
#define NUM_SENSOR 20 //кол-во датчиков
#define ONE_WIRE_BUS A4 //Z-pin
#define _powerPin 11 //главное реле
#define _resetPin A0
#define _clearRAMPin A1
#define _blockRAMPin A2
OneWire ds(ONE_WIRE_BUS);
Mux4051 Mux(A10, A9, A8, ONE_WIRE_BUS);
byte _arPinEn[] = { A7, A6, A5 };
byte _size_arPinEn = sizeof(_arPinEn) / sizeof(byte);
typedef struct {
	float min, max, calibr, temp;
	bool status, alarm, active;
	byte count_alarm;
	uint32_t startHeatTime, startAlarmTime;
} sensors_type;
byte _ERROR_ZONE[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
sensors_type _sensor[NUM_SENSOR];
int _HEAT = -1, _HEAT_ZONE = -1;
bool _ERROR_HA = false;
/******************  ******* ******************************/

#define START_DELAY 500
#define START_TIME_OUT 1000 //задержка при включении реле

int _Xx = 0, _Yy = 0, _keypressed = 0;
int _Xs = 0, _Ys = 0, _sleep_onOff = 0, _sleep = 0;
int _tickEventSetTemperature, _tickEventGetTemperature;

void drawAverageTemp(byte zone) {
	byte sector = 1, min_ = 0, max_ = 3, k = 0, i;
	float avg = 0;

	if (zone > 6) {
		sector = 3;
		min_ = 7;
		max_ = 9;
	} else if (zone > 3) {
		sector = 2;
		min_ = 4;
		max_ = 6;
	}

	for (i = min_; i <= max_; i++) {
		if (_sensor[i * 2].temp != -100 && i != _HEAT_ZONE && _ERROR_ZONE[zone] == 0) {
			avg += _sensor[i * 2].temp;
			k++;
		}
	}
	avg = avg / k;
	oTFT.setFont(DotMatrix_M);
	oTFT.setColor(C_YELLOW);

	int center, y = 350;

	oTFT.setColor(C_BG);
	switch (sector) {
	case 1:
		oTFT.fillRect(10, y - 5, 310, y + 20);
		center = 160;
		break;
	case 2:
		oTFT.fillRect(330, y - 5, 550, y + 20);
		center = 440;
		break;
	case 3:
		oTFT.fillRect(570, y - 5, 790, y + 20);
		center = 680;
		break;
	}
	oTFT.setColor(C_YELLOW);

	String str = (String) avg;
	byte len = str.length();

	uint8_t Ysize = oTFT.getFontYsize();
	uint8_t Xsize = oTFT.getFontXsize();

	int x = center - (len * Xsize / 2) + (len - 1) * 2;
	//if(!isnan(avg))
	if (avg)
		oTFT.printNumF(avg, 2, x, y);
}

void timer_func_set_temperature() {
	if (oTouch.dataAvailable() && (millis() - _keypressed) > START_DELAY) {
		if ((millis() - _keypressed) > (START_DELAY + 2000)) {
			_Xx = 0;
			_Yy = 0;
			_keypressed = millis();
		} else {
			oTouch.read();
			int x = oTouch.getX();
			int y = oTouch.getY();
			if (x > 5 && y > 5) {
				if (_Xx == 0 && _Yy == 0) {
					_Xx = x;
					_Yy = y;
					_keypressed = millis();
				} else if (_Xx > 0 && _Yy > 0 && abs(x-_Xx) >= 0 && abs(x-_Xx) <= 40 && abs(y-_Yy) >= 0 && abs(y-_Yy) <= 40) {
					byte z = getZone(x) + 1;
					byte l = getLevel(y) + 1;

					if (l == 1)
						tuning(z);
					_Xx = 0;
					_Yy = 0;
					_keypressed = millis();
				} else {
					_Xx = 0;
					_Yy = 0;
					_keypressed = millis();
				}
			}
		}
	}
}

void muxEnablePin(int pin) {
	byte m = pin / 8;
	for (byte i = 0; i < _size_arPinEn; i++) {
		if (i == m)
			digitalWrite(_arPinEn[i], LOW);
		else
			digitalWrite(_arPinEn[i], HIGH);
	}
	Mux.enablePin(pin % 8);
}

void getTemperature() {
	if (!_isProcessing) {
		_isProcessing = true;
		int level, zone, i, Tc_100;
		float tt;

		if (!_isOverHeatingTime && (int) (millis() - _startOverHeatingTime) > (_startOverHeatingTimePause * 60000))
			_isOverHeatingTime = true;
		for (i = 0; i < NUM_SENSOR; i++) {
			muxEnablePin(i);
			ds.reset_search();
			byte data[12];
			byte addr[8];

			level = i % 2; //0-воздух, 1-испаритель
			zone = i / 2;

			if (ds.search(addr)) {

				if (OneWire::crc8(addr, 7) != addr[7]) {
					//Serial.println(F("CRC is not valid!"));
				} else {
					if (addr[0] != 0x28) {
						//Serial.println(F("Device is not a DS18B20 family device."));
					} else {

						ds.reset();
						ds.select(addr);
						ds.write(0xBE);         // считываем ОЗУ датчика
						for (byte j = 0; j < 9; j++) {    // обрабатываем 9 байт
							data[j] = ds.read();
						}

						tt = calcTemp(data);

						if (tt < 85) {
							//_sensor[i].count_alarm = 0;
//							float ttC = tt + _sensor[i].calibr; //отключено 11.01.15
							float ttC = tt;
							drawTemp(zone, ttC, level);
							if (_startFan)
								drawAlarm(zone, level, false);

							//***********************************  запуск функции управления реле ********************************
							if (_startFan && _startSolenoid) {

								// ERROR_HA отключили 11.01.15
								/*if (level == 0 && _isOverHeatingTime) {
									if (_sensor[i].temp > (float) (_hysteresis + _sensor[i].min) && _sensor[i].startHeatTime == 0) {
										_sensor[i].startHeatTime = millis();
									}
									if (_sensor[i].temp < (float) (_hysteresis + _sensor[i].min + 10) && _sensor[i].startHeatTime > 0) {
										if ((int) (millis() - _sensor[i].startHeatTime) > _maxOverHeatingTime * 60000) {
											_sensor[i].startHeatTime = 0;
											drawAlarm(zone, 3, false);
											_ERROR_ZONE[zone] = _ERROR_ZONE[zone] & 0xFB;
										}
										_sensor[i].startHeatTime = 0;
									}

									if (_sensor[i].startHeatTime > 0 && (int) ((millis() - _sensor[i].startHeatTime)) > (int) (_maxOverHeatingTime * 60000)) {
										_ERROR_ZONE[zone] = _ERROR_ZONE[zone] || 0x4;
										drawAlarm(zone, 3, true);
									}
								}*/

								if (level == 0 && _HEAT_ZONE != zone && _ERROR_ZONE[zone] == 0) {
									switch (checkSolenoid(i)) {
									case -1:
										switchSolenoid(zone, LOW);
										break;
									case 1:
										switchSolenoid(zone, HIGH);
										break;
									}
								}

								if (level == 1 && _ERROR_ZONE[zone] == 0) {
									if (ttC < _sensor[i].min && _HEAT < 0) {
										_HEAT = i;
										_HEAT_ZONE = zone;
										_sensor[i].startHeatTime = millis();
										switchFan(zone, LOW);
										switchSolenoid(zone, LOW);
										delay(2000); //@todo Задержка между включение соленоидов нужна ли???
										sr595.writePin(zone * 3 + 3, HIGH);
										drawSnowFlake(zone * 80 + 40, L_C_3, 1);
										drawAverageTemp(zone);
									}

									if (_HEAT == i && (ttC > _sensor[i].max || ((millis() - _sensor[i].startHeatTime) / 60000) > _maxHeatTime)) {
										_HEAT = -1;
										_HEAT_ZONE = -1;

										sr595.writePin(zone * 3 + 3, LOW);
										drawSnowFlake(zone * 80 + 40, L_C_3, 0);
										switchFan(zone, HIGH);
										delay(2000); //@todo Задержка между включение соленоидов нужна ли???
										switchSolenoid(zone, HIGH);
									}
								}
							}
							//***********************************  запуск функции управления реле ********************************
						} else {
							//ошибка! инициализация датчика или больше 85С
							drawTemp(zone, -100, level);
							if (_startFan) {
								drawAlarm(zone, level, true);
							}
						}

						if (_start_) {
							ds.reset();
							ds.select(addr);
							ds.write(0x4E, 0x1F); //1F-10bit, 0x1F-9bit
						}
						ds.reset();
						ds.select(addr);
						ds.write(0x44, 1);
					}
				}
			} else {
				//ошибка! нету датчика!
				ds.reset_search();
				if (_startFan)
					drawAlarm(zone, level, true);
				drawTemp(zone, -100, level);
				switchSolenoid(zone, LOW);
			}
		}
		_start_ = false;
		_isProcessing = false;
	}
}
float calcTemp(uint8_t* data, byte resolution) {
	int neg = 1;
	int16_t Tc = ((int16_t) (data[1]) << 8) | data[0];

	if ((data[1] & 128) == 128) {
		neg = -1;
		Tc = ~Tc + 1;
	}
	switch (resolution) {
	case 12:
		return (float) Tc * 0.0625 * neg;
		break;
	case 11:
		return (float) (Tc >> 1) * 0.125 * neg;
		break;
	case 10:
		return (float) (Tc >> 2) * 0.25 * neg;
		break;
	case 9:
		return (float) (Tc >> 3) * 0.5 * neg;
		break;
	}
}

void switchFan(byte zone, byte status, bool draw) {
	status = constrain(status, 0x0, 0x1);
	sr595.writePin(zone * 3 + 1, status);
	if (draw)
		drawFan(zone * 80 + 40, L_C_1, status);
}
void switchSolenoid(byte zone, byte status, bool draw) {
	status = constrain(status, 0x0, 0x1);
	sr595.writePin(zone * 3 + 2, status);
	if (draw)
		drawSolenoid(zone * 80 + 40, L_C_2, status);
}
int checkSolenoid(byte i) {
	byte level = i % 2; //0-воздух, 1-испаритель
	byte zone = i / 2;
	int ret = 0;
	if (level == 0) {
		if (_sensor[i].temp < _sensor[i].min) {
			ret = -1;
		}
		if (_sensor[i].temp > (_sensor[i].min + _hysteresis)) {
			ret = 1;
		}
	}
	return ret;
}

int checkSolenoidHeat(byte i) {
	byte level = i % 2; //0-воздух, 1-испаритель
	byte zone = i / 2;
	int ret = 0;
	if (level == 1) {
		if (_sensor[i].temp < _sensor[i].min) {
			ret = 1;
		}
		if (_sensor[i].temp > _sensor[i].max) {
			ret = -1;
		}
	}
	return ret;
}

void loop() {
	t.update();
}

void timer_onOff() {
	if (oTouch.dataAvailable() && (millis() - _sleep_onOff) > START_DELAY) {
		if ((millis() - _sleep_onOff) > (START_DELAY + 1000)) {
			_Xs = 0;
			_Ys = 0;
			_sleep_onOff = millis();
		} else {
			oTouch.read();
			int x = oTouch.getX();
			int y = oTouch.getY();
			if (x > 700 && y > 425) {
				if (_Xs == 0 && _Ys == 0) {
					_Xs = x;
					_Ys = y;
					_sleep_onOff = millis();
				} else if (_Xs > 0 && _Ys > 0 && abs(x-_Xs) >= 0 && abs(x-_Xs) <= 40 && abs(y-_Ys) >= 0 && abs(y-_Ys) <= 40) {
					if (_working)
						digitalWrite(_resetPin, LOW);
					_working = !_working;
					drawButtonOnOff();
					_Xs = 0;
					_Ys = 0;
					_sleep_onOff = millis();
				} else {
					_Xs = 0;
					_Ys = 0;
					_sleep_onOff = millis();
				}
			}
		}

	}
}

void setup() {
//	Serial.begin(9600);			//@todo  Deleted!!!
	pinMode(_powerPin, OUTPUT);
	pinMode(_blockRAMPin, OUTPUT);
	digitalWrite(_blockRAMPin, LOW);

	oTFT.InitLCD();
	oTouch.InitTouch();
	oTouch.setPrecision(PREC_EXTREME);
	delay(1000);
	oFlash.begin();
	pinMode(_clearRAMPin, INPUT);
	if (digitalRead(_clearRAMPin) == HIGH) { //clear RAM
		clearRAM();
	}

	readDataFromEEPROM();
	delay(1000);
	oTFT.setBackColor(VGA_BLACK);
	oTFT.clrScr();

	if (drawNumPad())
		drawSetup();

	pinMode(_resetPin, OUTPUT);

	drawButtonOnOff();
	/******************  init MUX 4051 ******************************/
	for (int i = 0; i < _size_arPinEn; i++) {
		pinMode(_arPinEn[i], OUTPUT);
		digitalWrite(_arPinEn[i], HIGH);
	}
	/******************  init DS18B20 ******************************/
	int tickEventOnOff = t.every(100, timer_onOff);
}

void clearRAM() {
	digitalWrite(_blockRAMPin, HIGH);
	if (oFlash.ID_device != 0) {
		oFlash.waitForReady();
		oFlash.readPage(0x0);
		oFlash.eraseChip();
		delay(100);
	}
	digitalWrite(_blockRAMPin, LOW);
}

/**************************************************************************************/

bool drawNumPad() {
	char buffer[7] = { '_', '_', '_', '_', '_', '_', '\0' }, pass[7] = { '_', '_', '_', '_', '_', '_', '\0' };

	int W = 60, H = 60, sleep, pressed_button, Y = 100, DELTA = 20, clear, enter, center = 400, i = 0;
	oBut.setTextFont(Ubuntu);
	oBut.setButtonColors(VGA_WHITE, VGA_YELLOW, VGA_WHITE, 0x8000, 0xA000);

	oBut.addButton(center - W / 2, Y + (H + DELTA) * 3, W, H, "0");

	oBut.addButton(center - W / 2 - W - DELTA, Y, W, H, "1");
	oBut.addButton(center - W / 2, Y, W, H, "2");
	oBut.addButton(center + W / 2 + DELTA, Y, W, H, "3");

	oBut.addButton(center - W / 2 - W - DELTA, Y + H + DELTA, W, H, "4");
	oBut.addButton(center - W / 2, Y + H + DELTA, W, H, "5");
	oBut.addButton(center + W / 2 + DELTA, Y + H + DELTA, W, H, "6");

	oBut.addButton(center - W / 2 - W - DELTA, Y + (H + DELTA) * 2, W, H, "7");
	oBut.addButton(center - W / 2, Y + (H + DELTA) * 2, W, H, "8");
	oBut.addButton(center + W / 2 + DELTA, Y + (H + DELTA) * 2, W, H, "9");

	clear = oBut.addButton(center - W / 2 - W - DELTA, Y + (H + DELTA) * 3, W, H, "C");
	enter = oBut.addButton(center + W / 2 + DELTA, Y + (H + DELTA) * 3, W, H, "#");

	oBut.drawButtons();
	oTFT.setFont(Ubuntu);
	oTFT.setColor(VGA_WHITE);
	oTFT.setBackColor(C_BG);
	oTFT.print(buffer, 330, 50);

	bool enterSetup = false, ret = false, cancel = false;
	while (!enterSetup && !cancel) {
		if (oTouch.dataAvailable() && (millis() - sleep) > 100) {
			sleep = millis();

			pressed_button = oBut.checkButtons();

			switch (pressed_button) {
			case 10:
				i = 0;
				for (int k = 0; k < 6; k++) {
					buffer[k] = '_';
					pass[k] = '_';
				}
				oTFT.print(buffer, 330, 50);
				break;
			case 11:
				oTFT.print(buffer, 330, 50);
				enterSetup = true;
				for (int k = 0; k < 6; k++) {
					if (_PASS[k] != pass[k])
						enterSetup = false;
				}
				if (!enterSetup)
					cancel = true;
				break;
			case -1:
				break;
			default:
				if (i > 0)
					buffer[i - 1] = '*';
				pass[i] = pressed_button + 48;
				buffer[i] = pass[i];
				if (i < 5)
					i++;
				oTFT.print(buffer, 330, 50);
				break;
			}
		}
	}
	oBut.deleteAllButtons();
	oTFT.clrScr();
	return enterSetup;
}

/************************** drawFan **********************************/
void drawFan(int x, int y, int active) {
	int color = C_ACTIVE;
	if (active == 0)
		color = C_INACTIVE;

	oTFT.setColor(C_BG);
	oTFT.fillCircle(x, y, 36);
	oTFT.setColor(color);
	oTFT.fillCircle(x, y, 3);
	oTFT.drawCircle(x, y, 5);

	byte shift = 19;
	for (int i = 0; i < 360; i = i + 72) {
		oTFT.setColor(color);
		oGeo.drawPie(x, y, 29, i - shift, i + shift);
	}
}
/************************** drawSolenoid **********************************/
void drawSolenoid(int x, int y, bool active) {
	int color = C_ORANGE;
	if (!active)
		color = C_INACTIVE;

	oTFT.setColor(color);
	oTFT.drawCircle(x, y, 22);
	oTFT.drawCircle(x, y, 21);
	oTFT.drawLine(x - 16, y - 15, x + 21, y - 5);
	oTFT.drawLine(x - 16, y - 16, x + 21, y - 6);
	oTFT.drawLine(x - 16, y + 15, x + 21, y + 5);
	oTFT.drawLine(x - 16, y + 14, x + 21, y + 4);
}
/************************** drawSnowFlake **********************************/
void drawSnowFlake(int x, int y, byte active) {
	int color = C_CYAN;
	if (active == 0)
		color = C_INACTIVE;

	oTFT.setColor(C_BG);
	oTFT.fillCircle(x, y, 36);
	oTFT.setColor(color);
	boolean flag = false;
	for (int i = 0; i < 360; i = i + 30) {
		int shift = 2;
		if (flag)
			oGeo.drawPie(x, y, 12, i - shift, i + shift);
		else
			oGeo.drawPie(x, y, 17, i - shift, i + shift);
		flag = !flag;
	}
	for (byte i = 0; i < 5; i++) {
		int Yy = y + 25;
		if (i == 0 || i == 4)
			Yy = Yy - 7;
		else if (i == 1 || i == 3)
			Yy = Yy - 2;
		oTFT.drawCircle(x - 20 + i * 10, Yy, 2);
	}
}
/************************** drawSnowFlake **********************************/
int _tickEventStartFan = 0, _tickEventStartSol = 0;

void drawButtonOnOff() {
	int BG = oTFT.getBackColor();
	oTFT.setFont(DotMatrix_M);
	if (_working) {
		digitalWrite(_powerPin, LOW);
		oTFT.setColor(210, 0, 0);
		oTFT.drawRoundRect(700, 425, 795, 475);
		oTFT.setColor(80, 0, 0);
		oTFT.fillRoundRect(702, 427, 793, 473);
		oTFT.setColor(255, 255, 255);
		oTFT.setBackColor(80, 0, 0);
		oTFT.print("STOP", 717, 440);
		draw_interface();
		_tickEventGetTemperature = t.every(1000, getTemperature);
		delay(1000);
		_tickEventStartFan = t.every(START_TIME_OUT, startFan, 10);
		sr595.writePin(32, HIGH);
		_startOverHeatingTime = millis();
	} else {
		digitalWrite(_powerPin, HIGH);
		if (_tickEventSetTemperature)
			t.stop(_tickEventSetTemperature);
		t.stop(_tickEventGetTemperature);
		_start_ = true;
		if (_tickEventStartFan > 0)
			t.stop(_tickEventStartFan);
		if (_tickEventStartSol > 0)
			t.stop(_tickEventStartSol);
		_startFan = false;
		_startSolenoid = false;
		_isOverHeatingTime = false;
		sr595.init();
		oTFT.clrScr();
		oTFT.setColor(0, 210, 0);
		oTFT.drawRoundRect(700, 425, 795, 475);
		oTFT.setColor(0, 80, 0);
		oTFT.fillRoundRect(702, 427, 793, 473);
		oTFT.setColor(255, 255, 255);
		oTFT.setBackColor(0, 80, 0);
		oTFT.print("START", 708, 440);

		for (byte i = 0; i < NUM_SENSOR; i++) {
			_sensor[i].status = false;
			_sensor[i].alarm = false;
			_sensor[i].count_alarm = 0;
			_sensor[i].startAlarmTime = 0;
			_sensor[i].startHeatTime = 0;
		}
		sr595.writePin(32, LOW);
	}
	oTFT.setBackColor(BG);
}

void startFan() {
	bool loop = true;
	for (byte i = 0; i < 10; i++) {
		if (!_sensor[i * 2].status && loop) {
			if (!_sensor[i * 2].alarm) {
				_sensor[i * 2].status = true;
				int center = i * 80 + 40;
				//Включение реле вентилятора
				switchFan(i, HIGH);
			}
			loop = false;
			if (i == 9) {
				_startFan = true;
				_tickEventStartSol = t.every(START_TIME_OUT, startSol, 10);
			}
		}
	}
}
byte _nextStartSolenoid = 0;
void startSol() {
	byte i = _nextStartSolenoid;
	bool stat = checkSolenoid(i * 2);
	if (stat) {
		if (!_sensor[i * 2].alarm) {
			_sensor[i * 2].status = true;
			int center = i * 80 + 40;
			//Включение реле соленоида
			switchSolenoid(i, HIGH);
		}
	}
	if (i == 9) {
		_startSolenoid = true;
		_startOverHeatingTime = millis();
		_nextStartSolenoid = -1;
		_tickEventSetTemperature = t.every(100, timer_func_set_temperature);
	}
//	if(_nextStartSolenoid==-1 || _nextStartSolenoid==9)
	_nextStartSolenoid++;
}
void draw_interface() {
	oTFT.setBackColor(C_BG);
	oTFT.setFont(Ubuntu);
	for (byte i = 1; i <= 10; i++) {
		int center = i * 80 - 40;
		if (i == 4 || i == 7) {
			oTFT.setColor(64, 64, 64);
			oTFT.drawLine(i * 80, 2, i * 80, 380);
		}

		oTFT.setColor(C_RED);
		if (i == 10)
			center = center - 24;
		else
			center = center - 12;
		oTFT.printNumI(i, center, 15);
	}

	for (byte i = 1; i <= 10; i++) {
		int center = i * 80 - 40;
		drawFan(center, L_C_1, 0);
		drawSolenoid(center, L_C_2, false);
		drawSnowFlake(center, L_C_3, 0);
	}
}

void drawAlarm(byte zone, byte level, boolean alarm) {

	int y1, y2, y, center = zone * 80 + 40;
	if (level == 3) {
		if (alarm) {
			_ERROR_ZONE[zone] = 4;

			oTFT.setColor(VGA_RED);
			oTFT.fillCircle(center, L_C_2, 34);
			oTFT.setBackColor(VGA_RED);
			oTFT.setColor(C_BG);
			oTFT.drawCircle(center, L_C_2, 32);
			oTFT.setFont(Ubuntu);
			oTFT.print("HA", center - 24, L_C_2 - 10);
			oTFT.setBackColor(C_BG);
			//отключение реле
			if (!_sensor[zone * 2].alarm)
				switchFan(zone, LOW, true);
			else
				switchFan(zone, LOW, false);
			switchSolenoid(zone, LOW, true);
			sr595.writePin(zone * 3 + 3, LOW);
			sr595.writePin(31, HIGH);
			_ERROR_HA = true;
		}
	} else {

		if (_sensor[zone * 2 + level].alarm != alarm) {

			byte e = level + 1;

			switch (level) {
			case 0:
				y = L_C_1;
				break;
			case 1:
				y = L_C_3;
				break;
			}
			if (alarm && !_sensor[zone * 2 + level].alarm) {
				if (_sensor[zone * 2 + level].count_alarm > 10) {
					_sensor[zone * 2 + level].alarm = alarm;
					_ERROR_ZONE[zone] = 1;
					oTFT.setColor(VGA_RED);
					oTFT.fillCircle(center, y, 34);
					oTFT.setBackColor(VGA_RED);
					oTFT.setColor(C_BG);
					oTFT.drawCircle(center, y, 32);
					oTFT.setFont(Ubuntu);
					oTFT.print("E" + (String) e, center - 24, y - 10);
					oTFT.setBackColor(C_BG);
					//отключение реле
					if (!_sensor[zone * 2].alarm)
						switchFan(zone, LOW, true);
					else
						switchFan(zone, LOW, false);
					switchSolenoid(zone, LOW, true);
					sr595.writePin(zone * 3 + 3, LOW);
					if (zone == _HEAT_ZONE) {
						_HEAT_ZONE = -1;
						_HEAT = -1;
					}
					_sensor[zone * 2 + level].count_alarm = 0;
				}
				_sensor[zone * 2 + level].count_alarm++;
			} else {
				oTFT.setColor(C_BG);

				if (e == 1 && !_sensor[zone * 2 + 1].alarm) {
					_ERROR_ZONE[zone] = 0;
					_sensor[zone * 2 + level].alarm = alarm;
					_sensor[zone * 2 + level].count_alarm = 0;
					oTFT.fillCircle(center, y, 36);
					//включение реле
					switchFan(zone, _sensor[zone * 2].status, true);

					switch (checkSolenoid(zone * 2 + 1)) {
					case -1:
						switchSolenoid(zone, LOW);
						break;
					case 1:
						switchSolenoid(zone, HIGH);
						break;
					}

				} else if (e == 2 && !_sensor[zone * 2].alarm) {
					_ERROR_ZONE[zone] = 0;
					_sensor[zone * 2 + level].alarm = alarm;
					_sensor[zone * 2 + level].count_alarm = 0;
					oTFT.fillCircle(center, y, 36);
					//включение реле
					drawSnowFlake(center, y, 0);
					sr595.writePin(zone * 3 + 3, LOW);
					delay(2000);			//@todo ???
					switchFan(zone, HIGH, true);
					switchSolenoid(zone, HIGH, true);
				}

			}

			if (!_ERROR_HA) {
				bool alarmFlag = false;
				for (byte i = 0; i < NUM_SENSOR; i++) {
					if (_sensor[i].alarm)
						alarmFlag = true;
				}

				if (alarmFlag)
					sr595.writePin(31, HIGH);
				else
					sr595.writePin(31, LOW);
			}
		}
	}
}

void drawTemp(byte zone, float temp, byte level) {
	float abs_temp;
	int y, x, Ysize, Xsize, center;
	if (temp != 0)
		abs_temp = abs(temp);
	else
		abs_temp = 0;

	if (_sensor[zone * 2 + level].temp != temp || _start_) {
		oTFT.setFont(DotMatrix_M);
		Ysize = oTFT.getFontYsize();
		Xsize = oTFT.getFontXsize();
		String str = (String) abs_temp;
		byte len = str.length();
		byte sh;
		if (len > 3)
			sh = 2;
		else
			sh = 3;
		center = zone * 80 + 40;
		x = center - (len * Xsize / 2) + (len - 1) * sh;

		if (level == 0)
			y = L_C_1 + 36; //34
		else
			y = L_C_3 + 36; //33
		oTFT.setColor(C_BG);
		oTFT.fillRect(center - 38, y, center + 38, y + Ysize);
		if (temp < 0)
			oTFT.setColor(C_BLUE);
		else if (temp == 0)
			oTFT.setColor(VGA_WHITE);
		else if (temp > 0)
			oTFT.setColor(VGA_RED);

		if (temp != -100)
			oTFT.printNumF(abs_temp, 1, x, y);
		_sensor[zone * 2 + level].temp = temp;
		if (level == 0)
			drawAverageTemp(zone);
	}
}

void drawSetup() {
	char buffer[32][6];
	int pressed_button, center;
	byte i, k, j;

	oTFT.setBackColor(C_BG);
	oTFT.setFont(Ubuntu);

	for (i = 1; i <= 10; i++) {
		center = i * 80 - 40;
		if (i == 4 || i == 7) {
			oTFT.setColor(64, 64, 64);
			oTFT.drawLine(i * 80, 2, i * 80, 300);
		}
		oTFT.setColor(C_RED);
		if (i == 10)
			center = center - 24;
		else
			center = center - 12;
		oTFT.printNumI(i, center, 15);
	}

	oTFT.setFont(franklingothic_normal);
	oTFT.setColor(0x52BF);
	oTFT.print("calibration,`C", 10, 70);
	oTFT.print("min,`C", 10, 160);
	oTFT.setColor(0xFAAA);
	oTFT.print("max,`C", 10, 230);
	oTFT.setColor(VGA_WHITE);
	oTFT.print("hysteresis,`C", 10, 310);
	oTFT.print("maxHeatTime,(minutes)", 400, 310);

	oBut.setTextFont(SmallFont);
	oBut.setButtonColors(VGA_WHITE, VGA_YELLOW, VGA_WHITE, VGA_GRAY, 0x10A2);
	for (j = 0; j < 10; j++) {
		sprintf(buffer[j * 3], "%1.1f", _sensor[j * 2].calibr);
		sprintf(buffer[j * 3 + 1], "%1.1f", _sensor[j * 2 + 1].min);
		sprintf(buffer[j * 3 + 2], "%1.1f", _sensor[j * 2 + 1].max);
		oBut.addButton(j * 80 + 10, 90, 60, 40, buffer[j * 3]);
		oBut.addButton(j * 80 + 10, 180, 60, 40, buffer[j * 3 + 1]);
		oBut.addButton(j * 80 + 10, 250, 60, 40, buffer[j * 3 + 2]);
	}
	sprintf(buffer[30], "%1.1f", _hysteresis);
	oBut.addButton(10, 330, 60, 40, buffer[30]);
	sprintf(buffer[31], "%d", _maxHeatTime);
	oBut.addButton(400, 330, 60, 40, buffer[31]);

	int butMinus = -1, butPlus = -1, butOk, butCancel, lastB = -1;

	butCancel = oBut.addButton(225, 420, 80, 50, "OTMEHA");
	butOk = oBut.addButton(495, 420, 80, 50, "COXPAH");

	oBut.drawButtons();

	bool save = false;
	int level;
	float temp;
	byte s, n;
	while (!save) {
		if (oTouch.dataAvailable() == true) {
			pressed_button = oBut.checkButtons();
			if (pressed_button >= 0 && pressed_button <= 31) {
				oBut.disableButton(pressed_button, true);
				if (lastB >= 0)
					oBut.enableButton(lastB, true);
				lastB = pressed_button;

				if (butMinus > 0)
					oBut.deleteButton(butMinus);
				if (butPlus > 0)
					oBut.deleteButton(butPlus);

				butMinus = oBut.addButton(325, 420, 50, 50, "-");
				oBut.drawButton(butMinus);
				butPlus = oBut.addButton(425, 420, 50, 50, "+");
				oBut.drawButton(butPlus);

			} else if (pressed_button > 31) {
				level = lastB % 3;
				if (lastB == 30) {
					temp = _hysteresis;
				} else if (lastB == 31) {
					temp = _maxHeatTime;
				} else {
					if (lastB % 3 == 0) {
						k = lastB / 3 * 2;
					} else {
						k = lastB / 3 * 2 + 1;
					}

					if (level == 0)
						temp = _sensor[k].calibr;
					else if (level == 1)
						temp = _sensor[k].min;
					else
						temp = _sensor[k].max;
				}

				if (pressed_button == butMinus) {
					if (((lastB == 30 || lastB == 31) && temp > 1) || (lastB < 30 && temp > -25)) {
						if (lastB == 31)
							temp -= 1;
						else
							temp -= 0.5;
						if(level==2 && temp<0) temp=0;
					}
					if (lastB == 31)
						sprintf(buffer[lastB], "%d", (int) temp);
					else
						sprintf(buffer[lastB], "%1.1f", temp);
					oBut.relabelButton(lastB, buffer[lastB], true);
					if (lastB == 30) {
						_hysteresis = temp;
					} else if (lastB == 31) {
						_maxHeatTime = temp;
					} else {
						if (level == 0)
							_sensor[k].calibr = temp;
						else if (level == 1)
							_sensor[k].min = temp;
						else
							_sensor[k].max = temp;
					}
				} else if (pressed_button == butPlus) {
					if (temp < 25)
						if (lastB == 31)
							temp += 1;
						else
							temp += 0.5;
					if (level == 1 && temp>=0) temp=-0.5;
					if (lastB == 31)
						sprintf(buffer[lastB], "%d", (int) temp);
					else
						sprintf(buffer[lastB], "%1.1f", temp);
					oBut.relabelButton(lastB, buffer[lastB], true);
					if (lastB == 30) {
						_hysteresis = temp;
					} else if (lastB == 31) {
						_maxHeatTime = temp;
					} else {
						if (level == 0)
							_sensor[k].calibr = temp;
						else if (level == 1)
							_sensor[k].min = temp;
						else
							_sensor[k].max = temp;
					}
				} else if (pressed_button == butCancel) {
					save = true;
					oBut.deleteAllButtons();
					oTFT.setColor(C_BG);
					oTFT.clrScr();

				} else if (pressed_button == butOk) {
					digitalWrite(_blockRAMPin, HIGH);
					//запись данных в память
					if (oFlash.ID_device != 0) {

						oFlash.waitForReady();
						oFlash.readPage(0x0);
						oFlash.eraseChip();
						delay(100);
						for (i = 0; i < NUM_SENSOR; i++) {
							s = i * 2;
							n = i * 2 + 1;

							oFlash.buffer[n] = (int) abs(_sensor[i].calibr * 10);

							if (_sensor[i].calibr < 0)
								oFlash.buffer[s] = 1;
							else
								oFlash.buffer[s] = 0;

							s += 64;
							n += 64;
							oFlash.buffer[n] = (int) abs(_sensor[i].min * 10);

							if (_sensor[i].min < 0)
								oFlash.buffer[s] = 1;
							else
								oFlash.buffer[s] = 0;

							s += 64;
							n += 64;

							oFlash.buffer[n] = (int) abs(_sensor[i].max * 10);

							if (_sensor[i].max < 0)
								oFlash.buffer[s] = 1;
							else
								oFlash.buffer[s] = 0;

						}
						oFlash.buffer[254] = _maxHeatTime;
						oFlash.buffer[255] = _hysteresis * 10;
						oFlash.writePage(0);
						delay(100);
					}
					digitalWrite(_blockRAMPin, LOW);
					save = true;
					oBut.deleteAllButtons();
					oTFT.setColor(C_BG);
					oTFT.clrScr();
				}
			}
		}
	}
}

void tuning(byte zone) {
	float temp = _sensor[(zone - 1) * 2].min;
	float abs_temp = abs(_sensor[(zone - 1) * 2].min);

	char buffer[6];

	sprintf(buffer, "%1.1f", _sensor[(zone - 1) * 2].min);

	oBut.setTextFont(franklingothic_normal);
	oBut.setButtonColors(VGA_WHITE, VGA_YELLOW, VGA_WHITE, VGA_GRAY, 0x10A2);
	int butMinus = oBut.addButton(230, 420, 50, 50, "-");
	int input = oBut.addButton(300, 420, 90, 50, buffer);
	oBut.disableButton(input, true);
	int butPlus = oBut.addButton(410, 420, 50, 50, "+");
	int butCancel = oBut.addButton(80, 420, 100, 50, "OTMEHA");
	int butOk = oBut.addButton(510, 420, 100, 50, "COXPAH");
	int pressed_button;
	oBut.drawButtons();

	bool save = false;
	while (!save) {
		if (oTouch.dataAvailable() == true) {
			pressed_button = oBut.checkButtons();

			if (pressed_button == butMinus) {
				if (temp > -25)
					temp = temp - 0.5;
				sprintf(buffer, "%1.1f", temp);

				oBut.relabelButton(input, buffer);
			} else if (pressed_button == butPlus) {
				if (temp < 25)
					temp = temp + 0.5;
				sprintf(buffer, "%1.1f", temp);
				oBut.relabelButton(input, buffer);
			} else if (pressed_button == butCancel) {
				save = true;
				oBut.deleteAllButtons();
				oTFT.setColor(C_BG);
				oTFT.fillRect(80, 415, 610, 475);

			} else if (pressed_button == butOk) {

				save = true;
				oBut.deleteAllButtons();
				oTFT.setColor(C_BG);
				oTFT.fillRect(80, 415, 610, 475);
				_sensor[(zone - 1) * 2].min = temp;
				digitalWrite(_blockRAMPin, HIGH);
				//запись данных в память
				if (oFlash.ID_device != 0) {

					oFlash.waitForReady();
					oFlash.readPage(0x0);
					oFlash.eraseChip();
					delay(100);

					byte s = (zone - 1) * 4 + 64;
					byte n = (zone - 1) * 4 + 1 + 64;
					oFlash.buffer[n] = (int) (abs(temp) * 10);

					if (temp < 0)
						oFlash.buffer[s] = 1;
					else
						oFlash.buffer[s] = 0;

					oFlash.writePage(0);
					delay(100);
				}
				digitalWrite(_blockRAMPin, LOW);

			}
			oBut.drawButtons();
		}
	}

}

int getZone(int x) {
	byte zone = x / 80;
	return zone;
}

byte getLevel(int y) {
	int8_t level = 0;
	if ((L_C_1 - 40) < y && y < (L_C_1 + 40))
		level = 0;
	else if ((L_C_3 - 40) < y && y < (L_C_3 + 40))
		level = 1;
	else
		level = -1;
	return level;
}

/******************************* EEPROM **********************************************/
void readDataFromEEPROM() {
	if (oFlash.ID_device != 0) {
		byte data;
		float calibr, min, max;
		oFlash.waitForReady();
		oFlash.readPage(0x0);
		for (byte i = 0; i < NUM_SENSOR; i++) {
			byte s = i * 2;
			byte n = i * 2 + 1;
			data = oFlash.buffer[n];
			if (data != 255 && data != 0) {
				calibr = data / 10.00;
				if (oFlash.buffer[s] == 1)
					calibr *= -1;
				_sensor[i].calibr = calibr;
			} else
				_sensor[i].calibr = 0;

			if (i % 2 == 1)
				_sensor[i].calibr = 0;

			//загрузка гестерезиса и мин макс по датчикам

			s += 64;
			n += 64;
			data = oFlash.buffer[n];
			if (data != 255 && data != 0) {
				min = data / 10.00;
				if (oFlash.buffer[s] == 1)
					min *= -1;
			} else
				if(i%2==1) min=-5.0; else min=0; // 11.01.15

			_sensor[i].min = min;

			s += 64;
			n += 64;
			data = oFlash.buffer[n];
			if (data != 255 && data != 0) {
				max = data / 10.00;
				if (oFlash.buffer[s] == 1)
					max *= -1;
			} else
				max = 2.0; // 11.01.15

			_sensor[i].max = max;
			//инициализация
			_sensor[i].active = true;
			_sensor[i].count_alarm = 0;
			_sensor[i].startAlarmTime = 0;
			_sensor[i].startHeatTime = 0;
		}
		if (oFlash.buffer[254] != 255 && oFlash.buffer[254] != 0)
			_maxHeatTime = oFlash.buffer[254];
		if (oFlash.buffer[255] != 255)
			_hysteresis = oFlash.buffer[255] / 10;

	}
}
